package gol;

import javax.sound.midi.Soundbank;
import java.util.Arrays;
import java.util.Objects;

public class Frame {

     private boolean[][] cells;

    public Frame(int width, int height) {
        cells = new boolean[width][height];
    }

    @Override
    public String toString() {
        // return string that representing the frame
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        final Frame frame = (Frame) obj;
        int w = this.cells.length;
        int h = this.cells[1].length;

        if (obj instanceof Frame) {}

        int w2 = frame.cells.length;
        int h2 = frame.cells[1].length;

        if (w != w2 && h != h2){
            return false;
        }


        for (int x = 0 ; x <= w-1 ; x++){
            for (int y = 0 ; y <= h-1 ; y++){
                if ( frame.cells[x][y] && this.cells[x][y]){
                    continue;
                }else if(!frame.cells[x][y] && !this.cells[x][y]) {
                    continue;
                }
                return false;
            }
        }

        return true;
    }

    public Integer getNeighbourCount(int x, int y) {
        int neighbour = 0;
        int width = cells.length;
        int height = cells[1].length;

        //code taken from slides
        for (int xn = x - 1; xn <= x + 1; xn++) {
            if (xn < 0 || xn > width - 1) continue;
            for (int yn = y - 1; yn <= y + 1; yn++) {
                if (yn < 0 || yn > height - 1) continue;
                if (xn == x && yn == y) continue;
                if (cells[xn][yn])
                neighbour++ ;
            }
        }

        return neighbour;
    }

    public boolean isAlive(int x, int y) {
        if (!outOfBounds(x, y))
            return false;
        return cells[x][y];
    }

    public void markAlive(int x, int y) {
        if (outOfBounds(x, y))
        cells[x][y] = true;
    }

    public Frame nextFrame() {
        int w = cells.length;
        int h = cells[1].length;
        int countingNeighbours = 0;

        Frame newFrame = new Frame(w, h);

        for (int x = 0 ; x <= w-1 ; x++){

            for (int y = 0 ; y <= h-1 ; y++){
                countingNeighbours = this.getNeighbourCount(x,y);
                if (countingNeighbours < 2 || countingNeighbours > 4) continue;

                if ((countingNeighbours == 2  || countingNeighbours == 3) && this.isAlive(x,y)){
                    newFrame.markAlive(x,y);
                } else if (countingNeighbours  == 3){
                    newFrame.markAlive(x,y);
                }
            }
        }

        return newFrame;
    }

    public boolean outOfBounds(int x, int y){
        int w = cells.length-1;
        int h = cells[1].length-1;

        if (x > w && h > y && x < 0 && y < 0){
            return true;
        }

        return false;
    }

}