package gol;


import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

@SuppressWarnings("unused")
public class GameOfLifeTest {

    @Test
    public void cellsCanBeMarkedAlive() {
        Frame frame = new Frame(10,10);

        frame.markAlive(1,1);

        Assert.assertTrue(frame.isAlive(1, 1));
    }

    @Test
    public void cellOutOfBounds() {
        Frame frame = new Frame(10,10);

        frame.markAlive(-9,-11);

        Assert.assertFalse(frame.isAlive(-9, -11));
    }


    @Test
    public void countCellNeighbours() {
        Frame frame = new Frame(10,10);

        frame.markAlive(1,1);
        frame.markAlive(2,1);
        frame.markAlive(3,1);

        assertThat(frame.getNeighbourCount(2, 2), is(3));
    }

    @Test
    public void cellWithFewerThanTwoNeighboursDies() {
        Frame frame = new Frame(10,10);

        frame.markAlive(1,1);
        frame.markAlive(1,0);
        Frame newFrame = frame.nextFrame();

        Assert.assertFalse(newFrame.isAlive(1, 0));
    }

    @Test
    public void cellWithMoreThanThreeNeighboursDies() {
        Frame frame = new Frame(10,10);

        frame.markAlive(1,1);

        frame.markAlive(1,0);
        frame.markAlive(0,1);
        frame.markAlive(0,0);
        frame.markAlive(0,2);
        Frame newFrame = frame.nextFrame();

        Assert.assertFalse(newFrame.isAlive(1, 1));
    }

    @Test
    public void cellWithTwoNeighboursLives() {
        Frame frame = new Frame(10,10);

        frame.markAlive(1,1);

        frame.markAlive(1,0);
        frame.markAlive(0,1);

        Frame newFrame = frame.nextFrame();

        Assert.assertTrue(newFrame.isAlive(1, 1));
    }

    @Test
    public void cellWithThreeNeighboursBecomesAlive() {
        Frame frame = new Frame(10,10);

        frame.markAlive(0,2);
        frame.markAlive(0,0);
        frame.markAlive(2,2);

        Frame newFrame = frame.nextFrame();

        Assert.assertTrue(newFrame.isAlive(1, 1));
    }

    @Test
    public void StationaryPattern() {
        Frame frame = new Frame(10,10);

        frame.markAlive(1,1);
        frame.markAlive(2,2);
        frame.markAlive(2,1);
        frame.markAlive(1,2);

        Frame newFrame = frame.nextFrame();

        Assert.assertTrue(newFrame.equals(frame));
    }


    @Test
    public void blinkerPatternRepeatsItself() {
        Frame frame = new Frame(10,10);

        frame.markAlive(1,1);
        frame.markAlive(2,1);
        frame.markAlive(3,1);

        Frame newFrame = frame.nextFrame();
        newFrame = newFrame.nextFrame();

        Assert.assertTrue(newFrame.equals(frame));
    }





    // If you need some helper method eg. Frame.toString() then write test to them as well.

    // It is strongly advised to use TDD.
    // It usually shows from the result whether TDD was used or not.

}