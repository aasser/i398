package stack;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


/*
- loo pinu ja kontrolli, et selles on 0 elementi. DONE
- loo pinu, lisa (push) kaks elementi ja kontrolli, et selles on 2 elementi (size == 2). DONE
- loo pinu, lisa (push) kaks elementi, võta (pop) kaks elementi ja kontrolli, et pinus on 0 elementi. DONE
- loo pinu, lisa (push) kaks elementi, võta (pop) kaks elementi ja kontrolli, et need on needsamad lisatud elemendid. DONE
- loo pinu, lisa (push) kaks elementi, vaata pealmist elementi (peek) ja kontrolli, et see on õige element. DONE
- loo pinu, lisa (push) kaks elementi, vaata pealmist elementi ja kontrolli, et pinus on 2 elementi. DONE
- loo pinu, lisa (push) kaks elementi, vaata pealmist elementi, vaata uuesti pealmist elementi ja kontrolli, et see on seesama, mis esimesel korral. DONE
- loo pinu, võta (pop) üks element ja kontrolli, et pinu viskab IllegalStateException-i. DONE
- loo pinu, vaata pealmist elementi ja kontrolli, et pinu viskab IllegalStateException-i. DONE
*/


public class StackTest {

    @Test
    public void newStackHasNoElements() {
        Stack stack = new Stack(100);

        assertThat(stack.getSize(), is(0));
    }

    @Test
    public void newStackHasTwoElements() {
        Stack stack = new Stack(100);

        stack.push(4);
        stack.push(5);

        assertThat(stack.getSize(), is(2));
    }

    @Test
    public void StackHasNoElementsAfterPops() {
        Stack stack = new Stack(100);

        stack.push(4);
        stack.push(5);

        stack.pop();
        stack.pop();

        assertThat(stack.getSize(), is(0));
    }

    @Test
    public void StackPopedPushedElements() {
        Stack stack = new Stack(2);

        stack.push(5);
        stack.push(4);

        assertThat(stack.pop(), is(4));
        assertThat(stack.pop(), is(5));

    }

    @Test
    public void StackPeekPushedElement() {
        Stack stack = new Stack(2);

        stack.push(5);
        stack.push(4);

        assertThat(stack.peek(), is(4));


    }

    @Test
    public void StackPeekPushedElementAndGetSize() {
        Stack stack = new Stack(2);

        stack.push(5);
        stack.push(4);

        assertThat(stack.peek(), is(4));
        assertThat(stack.getSize(), is(2));


    }

    @Test
    public void StackPeekPeek() {
        Stack stack = new Stack(2);

        stack.push(5);
        stack.push(4);

        int esimene = stack.peek();

        assertThat(stack.peek(), is(esimene));

    }

    @Test
    public void StackPopExeption() {
        Stack stack = new Stack(2);

        assertThrows(IllegalStateException.class, () -> {stack.pop();});

    }

    @Test
    public void StackPeekExeption() {
        Stack stack = new Stack(2);

        assertThrows(IllegalStateException.class, () -> {stack.peek();});

    }


}


class Stack {
    Integer[] stack;
    int index = 0;


    public Stack(int i) {
          stack = new Integer[i];

    }


    void push(int i) {
        stack[index] = i;
        index++;
    }

    public int pop() {

        if (this.getSize() == 0) {
            throw new IllegalStateException();
        }

        return stack[--index];
    }

    public int peek() {

        if (this.getSize() == 0) {
            throw new IllegalStateException();
        }

            return stack[index -1];
    }

    public int getSize() {

        return index;
    }
}
