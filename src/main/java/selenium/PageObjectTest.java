package selenium;

import org.junit.jupiter.api.Test;
import selenium.pages.FormPage;
import selenium.pages.ListPage;
import selenium.pages.LoginPage;
import selenium.pages.MenuPage;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class PageObjectTest {

    private static final String USERNAME = "user";
    private static final String CORRECT_PASSWORD = "1";
    private static final String USERNAME2 = "user2";
    private static final String WRONG_PASSWORD = "2";
    private static final String CHECKUSER = USERNAME + ", " + CORRECT_PASSWORD;

    @Test
    public void loginFailsWithFalseCredentials() {
        LoginPage loginPage = LoginPage.goTo();

        loginPage = loginPage.logInExpectingFailure(USERNAME, WRONG_PASSWORD);

        assertThat(loginPage.getErrorMessage(), is(notNullValue()));
    }

    @Test
    public void loginSucceedsWithCorrectCredentials() {
        LoginPage loginPage = LoginPage.goTo();

        MenuPage menuPage = loginPage.logIn(USERNAME, CORRECT_PASSWORD);

        assertThat(loginPage.getInfoMessage(), is(notNullValue()));

        assertThat(menuPage.getLogOutButton(), is(notNullValue()));
    }

    @Test
    public void canLogOut() {
        LoginPage loginPage = LoginPage.goTo();
        MenuPage menuPage = loginPage.logIn(USERNAME, CORRECT_PASSWORD);

        menuPage.logOut();

        assertThat(loginPage.getInfoMessage(), is(notNullValue()));
    }

    @Test
    public void userExists() {
        LoginPage loginPage = LoginPage.goTo();

        MenuPage menuPage = loginPage.logIn(USERNAME, CORRECT_PASSWORD);

        ListPage listpage = menuPage.toUserList();

        assertThat(listpage.getFirstUser(), is(CHECKUSER));
    }

    @Test
    public void userCanBeAdded() {
        LoginPage loginPage = LoginPage.goTo();

        MenuPage menuPage = loginPage.logIn(USERNAME, CORRECT_PASSWORD);

        FormPage formPage = menuPage.toAddUserPage();

        formPage.addNewUser(USERNAME2, WRONG_PASSWORD);

        ListPage listpage = menuPage.toUserList();

        assertThat(listpage.getNewUser(), is(2));
    }



}