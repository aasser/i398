package selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MenuPage extends AbstractPage {

    public MenuPage(WebDriver driver) {
        super(driver);
        if (elementById("menu_page") == null) {
            throw new IllegalStateException("not on menu page");
        }
    }

    public String getLogOutButton() {
        WebElement element = elementById("log_out_link");
        return element == null ? null : element.getText();
    }

    public ListPage toUserList() {

        elementById("show_users_link").click();

        return new ListPage(driver);
    }

    public FormPage toAddUserPage() {

        elementById("add_user_link").click();

        return new FormPage(driver);
    }


    public LoginPage logOut() {
        elementById("log_out_link").click();
        return new LoginPage(driver);
    }
}