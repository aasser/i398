package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class ListPage extends AbstractPage{

    public ListPage(WebDriver driver) {
        super(driver);
        if (elementById("list_page") == null) {
            throw new IllegalStateException("not on list page");
        }
    }

    public String getFirstUser() {
        String username = null;
        String pass = null;

        List<WebElement> rows = elementById("user_list")
                .findElements(By.tagName("div"));

        for (WebElement row : rows) {
            username = row.getAttribute("username");
            pass = row.getAttribute("password");
            break;
        }

        return username + ", " + pass;
    }

    public int getNewUser() {
        List<String> username = new ArrayList<>();

        List<WebElement> rows = elementById("user_list")
                .findElements(By.tagName("div"));

        for (WebElement row : rows) {
            username.add(row.getAttribute("username"));

        }

        return username.size();
    }

}
