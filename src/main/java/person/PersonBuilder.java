package person;

public class PersonBuilder {
    private String name;
    private int age;
    private String gender;
    private Address address;

    public PersonBuilder withAge(int age) {
        this.age = age;
        return this;
    }

    public PersonBuilder withGender(String gender) {
        this.gender = gender;
        return this;
    }

    public PersonBuilder withTown(String town) {
        Address aadres = new Address(null, town);
        aadres.setTown(town);
        this.address = aadres;
        return this;
    }

    public Person build() {
        Person p = new Person(name, age, gender, address);
        p.setAddress(address);
        return p;
    }
}
