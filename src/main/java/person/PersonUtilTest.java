package person;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static java.util.Arrays.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.fail;

public class PersonUtilTest {

    private PersonUtil personUtil = new PersonUtil();

    @Test
    public void findsOldestPerson() {
        Person p1 = aPerson().withAge(32).build();
        Person p2 = aPerson().withAge(55).build();
        Person p3 = aPerson().withAge(21).build();

        assertThat(personUtil.getOldest(asList(p1, p2, p3)), is(p2));
    }

    @Test
    public void findsPersonsInLegalAge() {
        Person p1 = aPerson().withAge(32).build();
        Person p2 = aPerson().withAge(55).build();
        Person p3 = aPerson().withAge(15).build();

        assertThat(personUtil.getPersonsInLegalAge(asList(p1, p2, p3)), is(asList(p1,p2)));

    }

    @Test
    public void findsWomen() {
        Person p1 = aPerson().withGender("M").build();
        Person p2 = aPerson().withGender("M").build();
        Person p3 = aPerson().withGender("F").build();

        assertThat(personUtil.getWomen(asList(p1, p2, p3)), is(asList(p3)));
    }

    @Test
    public void findsPersonsLivingInSpecifiedTown() {
        Person p1 = aPerson().withTown("F").build();
        Person p2 = aPerson().withTown("M").build();
        Person p3 = aPerson().withTown("F").build();

        assertThat(personUtil.getPersonsWhoLiveIn("F", asList(p1,p2,p3)), is(asList(p1,p3)));
    }

    private PersonBuilder aPerson() {
        return new PersonBuilder();
    }
}
