package custommock;

import common.BankService;
import common.Money;

import java.util.ArrayList;
import java.util.List;

public class TestableBankService implements BankService {

    private boolean wasWithdrawCalled = false;

    private Money rememberWithdrawMoney;
    private String withdrawAccount;

    private Money rememberDeposiMoney;
    private String depositAccount;

    @Override
    public void withdraw(Money money, String fromAccount) {
        System.out.println("withdraw: " + money + " and " + fromAccount);
        wasWithdrawCalled = true;
        rememberWithdrawMoney = money;
        withdrawAccount = fromAccount;
    }

    @Override
    public void deposit(Money money, String toAccount) {
        System.out.println("deposit: " + money + " and " + toAccount);
        rememberDeposiMoney = money;
        depositAccount = toAccount;
    }

    public boolean wasWithdrawCalledWith(Money money, String account) {
        System.out.println("withdraw called with: " + rememberWithdrawMoney + " and " + withdrawAccount);
        System.out.println("withdraw called with: " + money + " and " + account);

        if (money == (null) && account == (null))
        {return false;}

        return money.equals(rememberWithdrawMoney) && account.equals(withdrawAccount);

    }

    public boolean wasDepositCalledWith(Money money, String account) {
        System.out.println("deposit called with: " + rememberDeposiMoney + " and " + depositAccount);

        if (money == (null) && account == (null))
            return false;

        return money.equals(rememberDeposiMoney) && account.equals(depositAccount);
    }

    @Override
    public Money convert(Money money, String targetCurrency) {
        if (money.getCurrency().equals(targetCurrency)) return money;

        double rate = 1.0/10;

        return new Money((int) (money.getAmount() / rate), targetCurrency);
    }

    @Override
    public String getAccountCurrency(String account) {
        switch (account) {
            case "E_123": return "EUR";
            case "S_456": return "SEK";
            default: throw new IllegalStateException();
        }
    }

    @Override
    public boolean hasSufficientFundsFor(Money requiredAmount, String account) {
        return true;
    }

    public void setSufficientFundsAvailable(boolean areFundsAvailable) {
        //at the moment hasSufficientFundsFor() returns always true.
        //this method is for configuring how hasSufficientFundsFor() responds.
    }

    public boolean wasWithdrawCalled() {

    if (wasWithdrawCalled) {return false;}

        return true;

    }

}