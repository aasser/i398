package refactoring;

public class LineEndPoint {
    private final int x2;
    private final int y2;
    private final int z2;

    public LineEndPoint(int x2, int y2, int z2) {
        this.x2 = x2;
        this.y2 = y2;
        this.z2 = z2;
    }

    public int getX2() {
        return x2;
    }

    public int getY2() {
        return y2;
    }

    public int getZ2() {
        return z2;
    }
}
