package refactoring;

import java.util.*;

public class Refactoring {

    // 1a done
    public int increaseInvoiceNumber() {
        return ++invoiceNumber;
    }

    // 1b done
    public void findFilledOrders(List<Order> orderList) {
        for (Order order : orders) {
            if (order.isFilled()) {
                orderList.add(order);
            }
        }
    }

    // 2a done
    public void printStatementFor(Long invoiceId) {
        List<InvoiceRow> invoiceRows = dao.getInvoiceRowsFor(invoiceId);

        printInvoiceRows(invoiceRows);
        calculateInvoiceTotal(invoiceRows);

    }

    private void calculateInvoiceTotal(List<InvoiceRow> invoiceRows) {
        // calculate invoice total
        double total = 0;
        for (InvoiceRow invoiceRow : invoiceRows) {
            total += invoiceRow.getAmount();
        }

        printValue(total);
    }

    // 2b done
    public String getItemsAsHtml() {
        String result = "";
        result += "<ul>";

        for (String item : items){
            result += createTag("li",item);
        }

        result += "</ul>";
        return result;
    }

    private String createTag(String tag, String index) {

        String result = "<" + tag + ">" + index + "</" + tag + ">";

        return result;
    }

    // 3 done
    public boolean isSmallOrder() {
        return (order.getTotal() > 100);
    }

    // 4 done?
    static final double VAT = 1.2;
    public void printPrice() {
        System.out.println("Price not including VAT: " + basePrice());
        System.out.println("Price including VAT: " + basePrice() * VAT);
    }

    private double basePrice() {
        return getBasePrice();
    }

    // 5 done
    public void calculatePayFor(Job job) {
        // on holiday at night
        final boolean onHoliday = job.day == 6 || job.day == 7;
        final boolean atNight = job.hour > 20 || job.hour < 7;

        if (onHoliday && atNight) {
        }
    }

    // 6 done
    public boolean canAccessResource(SessionData sessionData) {
        // is admin and has preferred status

        return adminHasPreferedStatus(sessionData);
    }

    private boolean adminHasPreferedStatus(SessionData sessionData) {
        return (
                (sessionData.getCurrentUserName().equals("Admin")
                        || sessionData.getCurrentUserName().equals("Administrator"))
                        && (sessionData.getStatus().equals("preferredStatusX")
                        || sessionData.getStatus().equals("preferredStatusY"))
        );
    }

    // 7 done
    public void drawLines() {
        Space space = new Space();
        space.drawLine(new LineStartPoint(12, 3, 5), new LineEndPoint(2, 4, 6));
        space.drawLine(new LineStartPoint(2, 4, 6), new LineEndPoint(0, 1, 0));
    }

    // 8
    public int calculateWeeklyPay(int hoursWorked, boolean overtime) {
        int straightTime = Math.min(40, hoursWorked);
        int overTime = Math.max(0, hoursWorked - straightTime);
        int straightPay = straightTime * hourRate;
        double overtimeRate = overtime ? 1.5 * hourRate : 1.0 * hourRate;
        int overtimePay = (int) Math.round(overTime * overtimeRate);
        return straightPay + overtimePay;
    }

    // //////////////////////////////////////////////////////////////////////////

    // Helper fields and methods.
    // They are here just to make code compile

    private List<String> items = Arrays.asList("1", "2", "3", "4");
    private int hourRate = 5;
    int invoiceNumber = 0;
    private List<Order> orders = new ArrayList<>();
    private Order order = new Order();
    private Dao dao = new SampleDao();
    private double price = 0;

    void justACaller() {
        increaseInvoiceNumber();
        findFilledOrders(null);
    }

    private void printValue(double total) {
    }

    private void printInvoiceRows(List<InvoiceRow> invoiceRows) {
    }

    class Space {
        public void drawLine(LineStartPoint lineStartPoint, LineEndPoint lineEndPoint) {
        }

    }

    interface Dao {
        List<InvoiceRow> getInvoiceRowsFor(Long invoiceId);
    }

    class SampleDao implements Dao {
        @Override
        public List<InvoiceRow> getInvoiceRowsFor(Long invoiceId) {
            return null;
        }
    }

    class Order {
        public boolean isFilled() {
            return false;
        }

        public double getTotal() {
            return 0;
        }
    }

    class InvoiceRow {
        public double getAmount() {
            return 0;
        }
    }

    class Job {
        public int hour;
        public int day;
    }

    private double getBasePrice() {
        return price;
    }

    private class SessionData {

        public String getCurrentUserName() {
            return null;
        }

        public String getStatus() {
            return null;
        }

    }

}
