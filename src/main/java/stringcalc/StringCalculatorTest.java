package stringcalc;

import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StringCalculatorTest {

    // "" -> 0
    // "1" -> 1
    // "1, 2" -> 3
    // null -> IllegalArgumentException

    @Test
    public void emptyString_returnsZero() {
         StringCalculator c = new StringCalculator();

         int result = c.add("");

         assertThat(result, is(0));
    }

    @Test
    public void numberIsNumber() {
        StringCalculator c = new StringCalculator();

        int result = c.add("1");

        assertThat(result, is(1));
    }

    @Test
    public void numberIsNull() {
        StringCalculator c = new StringCalculator();

        assertThrows(IllegalStateException.class, () -> {c.add(null);});

    }

    @Test
    public void multipleNumberSum() {
        StringCalculator c = new StringCalculator();

        int result = c.add("1, 2");

        assertThat(result, is(3));
    }

}

class StringCalculator {


    public int add(String s) {

        if (s == null) {
            throw new IllegalStateException();
        }

//        if (s == "") {
//            return 0;
//        }

        if ("".equals(s)){
            return 0;
        }

        String[] numbers = s.split(", ?");
        int summary = 0;

        for (String number : numbers) {
            summary += Integer.parseInt(number);
        }
        return summary;
    }
}
