package rpn;

import org.junit.jupiter.api.Test;
import java.util.Stack;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;




public class RpnCalculatorTest {

    @Test
    public void newCalculatorHasZeroInItsAccumulator() {
        RpnCalculator c = new RpnCalculator();

        assertThat(c.getAccumulator(), is(0));
    }

    @Test
    public void AccumulatorCanBeSet() {
        RpnCalculator c = new RpnCalculator();

        c.setAccumulator(1);

        assertThat(c.getAccumulator(), is(1));
    }

    @Test
    public void AdditionCanBeDone() {
        RpnCalculator c = new RpnCalculator();

        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();

        assertThat(c.getAccumulator(), is(3));
    }

    @Test
    public void SubtractionCanBeDone() {
        RpnCalculator c = new RpnCalculator();

        c.setAccumulator(3);
        c.enter();
        c.setAccumulator(1);
        c.minus();

        assertThat(c.getAccumulator(), is(2));
    }

    @Test
    public void MultiplicationCanBeDone() {
        RpnCalculator c = new RpnCalculator();

        c.setAccumulator(3);
        c.enter();
        c.setAccumulator(1);
        c.multiply();

        assertThat(c.getAccumulator(), is(3));
    }

    @Test
    public void AdditionBeforeMultiply() {
        RpnCalculator c = new RpnCalculator();

        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        c.enter();
        c.setAccumulator(4);
        c.multiply();

        assertThat(c.getAccumulator(), is(12));
    }

    @Test
    public void MultipleAdditionsBeforeMultiplying() {
        RpnCalculator c = new RpnCalculator();

        c.setAccumulator(4);
        c.enter();
        c.setAccumulator(3);
        c.plus();
        c.enter();

        c.setAccumulator(2);
        c.enter();
        c.setAccumulator(1);
        c.plus();

        c.multiply();

        assertThat(c.getAccumulator(), is(21));
    }


    @Test
    public void EvaluationCanBeDone() {
        RpnCalculator c = new RpnCalculator();

        //String input = "5 1 +";

        assertThat(c.evaluate("5 1 2 + 4 * + 3 + "), is(20));
    }



}

class RpnCalculator {

    private int accomulator = 0;
    private Stack<Integer> stack = new Stack();

    public Integer getAccumulator() {

        return accomulator;
    }

    void setAccumulator(int accomulator){

        this.accomulator = accomulator;

    }

    void enter(){
        stack.push(this.getAccumulator());

    }

    void plus(){

        int firstOperand = stack.pop();
        int secondOperand = getAccumulator();
        int sum = firstOperand + secondOperand;

        //stack.push(sum);
        this.setAccumulator(sum);

    }

    void minus(){

        int firstOperand = stack.pop();
        int secondOperand = getAccumulator();
        int substract = firstOperand - secondOperand;

        //stack.push(substract);
        this.setAccumulator(substract);

    }

    void multiply(){

        int firstOperand = stack.pop();
        int secondOperand = getAccumulator();
        int multiply = firstOperand * secondOperand;

        //stack.push(multiply);
        this.setAccumulator(multiply);

    }

    //5 1 +
    int evaluate(String expression){

        for (String elem : expression.split(" ")){
            if("+".equals(elem)){

                plus();

            } else if("*".equals(elem)){
                multiply();
            } else if("-".equals(elem)){
                minus();
            } else {

                enter();

                setAccumulator(Integer.parseInt(elem));


            }
        }

        return accomulator;
    }

}