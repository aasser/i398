package string;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class OrderService {

    private DataSource dataSource;

    public OrderService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Order> getFilledOrders() {

        List<Order> orders1 = new ArrayList<>();

        List<Order> orders = this.dataSource.getOrders();

        for (Order order : orders) {
            if (order.isFilled()){
                orders1.add(order);
            }
        }

        return orders1;
    }

    public List<Order> getOrdersOver(double amount) {
        List<Order> orders1 = new ArrayList<>();
        List<Order> orders = this.dataSource.getOrders();

        for (Order order : orders){
            if (order.getTotal() > 100.0){
                orders1.add(order);
            }
        }

        return orders1;

    }

    public List<Order> getOrdersSortedByDate() {

        List<Order> orders = this.dataSource.getOrders();

        orders.sort(Comparator.comparing(Order::getOrderDate));
        //orders.sort(Comparator.comparing(Order::getOrderDate).reversed());

        return orders;
    }

}
