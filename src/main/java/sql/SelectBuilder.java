package sql;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SelectBuilder {

    List<String> col = new ArrayList<String>();
    List<String> tbls = new ArrayList<String>();
    List<String> wher = new ArrayList<String>();
    List<Object> paramList = new ArrayList<Object>();

    List<String> subCols = new ArrayList<String>();
    List<String> subTbls = new ArrayList<String>();
    List<String> equalsIfNotNull = new ArrayList<String>();
    List<String> leftJoins = new ArrayList<String>();
    List<String> ins = new ArrayList<String>();



    public String getSql() {

        StringBuilder sql = new StringBuilder();

        sql.append("select ")
                .append(String.join(", ", col))
                .append(" from ")
                .append(String.join(", ", tbls));

        if (!subCols.isEmpty() && !subTbls.isEmpty()) {
            sql.append("(select ")
                    .append(String.join(", ", subCols))
                    .append(" from ")
                    .append(String.join(", ", subTbls))
                    .append(")");
        }

        if (!wher.isEmpty()) {
            sql.append(" where ");
            sql.append(String.join(" and ", wher));
        }

        if (!equalsIfNotNull.isEmpty()) {
            sql.append(" where ");
            sql.append(String.join(" = ? and ", equalsIfNotNull));
            sql.append(" = ?");
        }

        if (!ins.isEmpty() && !paramList.isEmpty()){
            sql.append(" where ");
            sql.append(String.join(" = ", ins));
            sql.append(" in (");

            String listOfParameters = Stream.of(new String[paramList.size()])
                    .map(each -> "?")
                    .collect(Collectors.joining(", "));
            sql.append(listOfParameters);
            sql.append(")");
        }

        if (!leftJoins.isEmpty()){
            sql.append(" left join ");
            sql.append(String.join(" on ", leftJoins));
        }

        String query = sql.toString();
        return query;
    }


    public void column(String column) {
        col.add(column);
    }

    public void columns(String... columns) {

        for (String s : columns) {
            col.add(s);
        }
    }

    public void from(String table) {
        tbls.add(table);
    }

    public void where(String condition, Object parameter) {
        wher.add(condition);
        paramList.add(parameter);
    }

    public void where(String condition) {
        wher.add(condition);
    }

    public List<Object> getParameters() { return paramList; }

    public void eqIfNotNull(String column, Object parameter) {
        if (parameter != null){
            equalsIfNotNull.add(column);
            paramList.add(parameter);
        }

    }

    public void leftJoin(String table, String condition) {
        leftJoins.add(table);
        leftJoins.add(condition);
    }

    public void in(String column, List<Object> parameters) {
        ins.add(column);
        paramList.addAll(parameters);
    }

    public void from(SelectBuilder sub) {
        subCols.addAll(sub.col);
        subTbls.addAll(sub.tbls);
    }
}